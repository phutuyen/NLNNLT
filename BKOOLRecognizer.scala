

// Project name MP
// Create by: xcode
// Create date: Aug 27, 2012
// Language: Scala
// Description: This file is about recognizer for MP language, see MP language specification for more information

import scala.util.matching.Regex
import scala.util.parsing.combinator.lexical.StdLexical
import scala.util.parsing.combinator.token.StdTokens
import scala.util.parsing.input.CharArrayReader.EofCh
import scala.util.parsing.combinator.token.StdTokens
import scala.util.parsing.combinator.syntactical.StdTokenParsers
import scala.dbc.statement.Expression
import scala.dbc.statement.Expression
import scala.dbc.statement.Expression
import scala.dbc.statement.Expression
import scala.dbc.statement.Expression
import scala.dbc.statement.Expression
import scala.dbc.statement.Statement
import scala.dbc.statement.Expression
import scala.dbc.statement.Expression
import scala.dbc.statement.Expression
import com.sun.org.apache.xalan.internal.xsltc.compiler.Expression
import scala.dbc.statement.Expression
trait BKOOLTokens extends StdTokens {
  // Adapted from StdTokens
  case class FloatLit(chars: String) extends Token {
    override def toString = "FloatLit: "+chars
  }
  case class IntLit(chars: String) extends Token {
    override def toString = "IntLit: "+chars
  }
  case class BooleanLit(chars: String) extends Token {
    override def toString = "BooleanLit: " + chars
  }
  case class ErrorNoClose(chars: String) extends Token{
    override def toString ="Unclosed string: " + chars
  }
  case class IllegalTab(chars: String) extends Token{
    override def toString="Illegal tab in string: "+chars
  }
	case class unclosedcommet(chars:String) extends Token{
    override def toString = "unclosed comment"  
  }
}

// TODO Copy your lexer here
class BKOOLLexer extends StdLexical with BKOOLTokens {
  import scala.util.parsing.input.CharArrayReader.EofCh
  def regex(r: Regex): Parser[String] = new Parser[String] {
    def apply(in: Input) = {
      val source = in.source
      val offset = in.offset
      (r findPrefixMatchOf (source.subSequence(offset, source.length))) match {
        case Some(matched) =>
          Success(source.subSequence(offset, offset + matched.end).toString,
            in.drop(matched.end))
        case None =>
          Failure("string matching regex `" + r + "' expected but `" + in.first + "' found", in.drop(0))
      }
    }
  }

  // TODO student add code here to complete token
  reserved ++= List( "bool", "break", "class","continue","do","abstract","downto","else","extends","float","for","if","integer","new","repeat","string","then","to","until","while","return","true","abstract","false","void","null","self","final")

  delimiters ++= List(":","::", ";","(",")", "{", "}","=","[","]",".",",","+","*","\\",":=","<","<=","<>","!","^","-","/","%","==",">",">=","&&","||")

  override def token: Parser[Token] = {
    // Adapted from StdLexical
    (
    regex("true|false".r) 	^^ { BooleanLit(_)} 
	|regex ("""\(\*((.)|(\n))*""".r) ^^ {unclosedcommet(_)}
    |regex("([0-9]+)(((\\.[0-9]+)?((e|E)(\\+|-)?[0-9]+))|(\\.[0-9]*))".r)^^ { FloatLit(_) }
	|regex("[0-9]+".r)			^^ { IntLit(_) }
	|regex("[A-Za-z_][A-Za-z0-9_]*".r)  		^^ {processIdent(_)}
	|'\"' ~ rep( chrExcept('\"', '\n', EofCh,'\t'))~'\t'~rep( chrExcept('\"', '\n', EofCh,'\t'))~'\"' ^^{case '\"'~chars~'\t'~_~'\"' =>ErrorToken("Illegal tab in string: \""+(chars mkString ""))}
    | '\"' ~ rep( chrExcept('\"', '\n', EofCh) ) ~ '\"' ^^ { case '\"' ~ chars ~ '\"' => StringLit(chars mkString "") }
    | '\"' ~ rep( chrExcept('\"', '\n', EofCh) ) ^^{case '\"'~chars => ErrorToken("Unclosed string: \""+(chars mkString "")) }
    |EofCh 						^^^ EOF
    |delim

	)
  }

 override def whitespace: Parser[Any] = rep(
    whitespaceChar
    | '#' ~ rep(chrExcept(EofCh, '\n'))
    | '(' ~ '*' ~ comment
    | '(' ~ '*' ~ failure("unclosed comment"))

  override protected def comment: Parser[Any] = (
    '*' ~ ')' ^^ { case _ => ' ' }
    | chrExcept(EofCh) ~ comment)

}

class BKOOLRecognizer extends StdTokenParsers {
  type Tokens = BKOOLTokens
  val lexical = new BKOOLLexer
  def getLexical: BKOOLLexer = lexical

  def show(result: ParseResult[Any]): String = {
    result match {
      case Failure(msg, next) =>
        "Error at line "+ next.pos.line +" col "+ next.pos.column
	  case Error(msg,next) => 
		"Fatal error at line "+ next.pos.line +" col "+ next.pos.column
      case _ => "Successful"
    }
  }  
  
  def parse(s:String) = phrase(program)(new lexical.Scanner(s))
 // For BKOOLRecognizer, students may modify the following.
  def program: Parser[Any] =rep(classDeclaration)
  
  def classDeclaration: Parser[Any]="class"~ident~opt("extends"~ident)~classBody
  
  def classBody: Parser[Any]= "{"~rep(classBodyDeclaration)~"}"
  
  //class body
  
  def classBodyDeclaration:Parser[Any]=block|memberDeclaration

  def memberDeclaration: Parser[Any]=VMDeclaration | constantDeclaration |abstractMethodDeclaration
  
  def VMDeclaration: Parser[Any]=ident~A|Type~B|"void"~B
  
  def B:Parser[Any]=ident~"("~listParameter~")"~block
  
  def A:Parser[Any]=rep(","~ident)~":"~Type~";"|(ident~"("~listParameter~")"~block)|"("~listParameter~")"~block
    
  def abstractMethodDeclaration: Parser[Any]="abstract"~(Type|"void")~ident~"("~listParameter~")"~";"
  
  def listParameter:Parser[Any]=opt((rep1sep(ident,",")~":"~Type)~rep(";"~ident~":"~Type))
  
  def constantDeclaration:Parser[Any]="final"~Type~ident~"="~Expression~";"

  //Block
  
  
  def block: Parser[Any]="{"~rep(khaibao)~rep(statement)~"}"
  def khaibao:Parser[Any]=kbbien|constantDeclaration
  def kbbien:Parser[Any]=rep1sep(ident,",")~":"~Type~";"
  def statement:Parser[Any]=(
    block
    |ident~"." ~ ident ~ "(" ~ ListExpression ~ ")"~";"
    |"if"~Expression~"then"~statement~rep("else"~statement)
    |"while"~Expression~"do"~statement
    |Expression~ ":=" ~ Expression ~ ";"
    |"repeat"~rep1(statement)~"until"~Expression~";"
    |"for"~ident~":="~Expression~("to"|"downto")~Expression~"do"~statement
    |"break"~";"
    |"continue"~";"
    |"return"~Expression~";"
  )
  //Type

  def Type:Parser[Any]=(literalType|ident)~opt("["~intlit~"]")
  
  def literalType:Parser[Any]="string"|"bool"|"integer"|"float"
  // Expression
   def Expression : Parser[Any]= Expression1 ~ opt(("<"|">"|"<="|">=") ~ Expression1) 
      def Expression1 :Parser[Any] = Expression2 ~ opt(("=="|"<>") ~ Expression2)
      def Expression2 : Parser[Any]= Expression3 ~rep(("&&"|"||") ~ Expression3)
      def Expression3: Parser[Any] = Expression4 ~ rep(("+"|"-") ~ Expression4)
      def Expression4 : Parser[Any]= Expression5 ~ rep (("*"|"/"|"\\"|"%") ~ Expression5)
      def Expression5 : Parser [Any]= Expression6 ~ rep(("^") ~ Expression6)
      def Expression6 : Parser [Any] = Expression7 ~ opt(("!") ~ Expression7)
      def Expression7 : Parser[Any]= Expression8 ~ opt(("+"|"-")~ Expression8)
      def Expression8 : Parser[Any]= Expression9 ~ opt(("["~ Expression ~ "]"))
      def Expression9 : Parser[Any]= Expression10 ~ rep("." ~ ident ~ opt("(" ~ ListExpression ~ ")"))
      def Expression10 :Parser[Any]= "new" ~ ident ~ "(" ~ repsep(Expression,",") ~ ")" | "(" ~ Expression ~ ")" | operand
      def operand :Parser[Any]= ident|floatlit|stringlit|intlit|boollit |seft
      def seft : Parser [Any] = "self" ~ "." ~ ident ~ opt("(" ~ repsep(Expression, ",") ~ ")")
  
  //ListExpression
  
  def ListExpression: Parser[Any]=repsep(Expression,",")
  
  //Literal
  def boollit: Parser[Any] = elem("boolean", _.isInstanceOf[lexical.BooleanLit])
  
  def floatlit: Parser[Any] = elem("float", _.isInstanceOf[lexical.FloatLit]) 

  def intlit: Parser[Any] = elem("integer", _.isInstanceOf[lexical.IntLit])
  
  def stringlit: Parser[Any] = elem("stirng", _.isInstanceOf[lexical.StringLit])
 }
